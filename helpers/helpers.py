""" Set of Function Helpers

by JV
11/06/2021
"""

import json, random

def read_json_data(path):
    """ Reads a JSON file and returns a data representation in a python dict

    Args:
        path (string): File JSON path

    Returns:
        [json_data]: Data in dict format
    """

    data = open(path, 'r')
    json_data = json.load(data)
    return json_data


def try_catch(pokemon):
    """Tries catch a Pokemon in function of the captured rate and a random
    number. 
    If 1 <= 1 <= rate: Pokemon Catched ! =)
    else: The Pomenon is gone =(

    Args:
        pokemon ([Pokemon]): Wild Pokemon to capture
    Returns:
        [bool]: Is captured 
    """
    n_random = random.choice([n for n in range(1,256)])
    return 1 <= n_random <= pokemon.capture_rate
