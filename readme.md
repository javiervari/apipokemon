# Practical test - DjangoREST
This repo contains the answers to Turpial's Practical Test.



# Main objective

- Measure the level of knowledge and use of the framework.
- Evaluate autonomy and efficiency when executing a complex task.
- Test creativity and ingenuity when facing great challenges.
- Evaluate the ability to maintain a clear image of the final product objective for
extended periods of development.



# Technologies/Frameworks
- Python 3.6.9
- Django 3.2.4
- djangorestframework 3.12.4



# Project setup



## App Folder and Virtual Enviroment
Create App Folder and Virtual Enviroment

```bash
cd /my/projects/path/
mkdir javier_test
cd javier_test
virtualenv envAPIPokemon -p python3
source envAPIPokemon/bin/activate
```



## Clone Repo in local

```bash
git clone https://gitlab.com/javiervari/apipokemon.git
```



## Install dependencies
```bash
cd apipokemon
pip install -r requirements.txt
```



## Create and configure a local Data Base in PostgreSQL

```bash
sudo su postgres

postgres$ createuser --interactive --pwprompt #If not exist (All options: Yes)

postgres$ createdb pokemondb
postgres$ psql pokemondb

pokemondb$ alter user <usuario> with password '<password>';

```


## Configure the secret_file.json File:
I sent an email with a json file attached (secret.json). In this file should be all project secret info. In this case has the django secret key and DB attributes that will fill by you.
Please put this `secret.json` in the root path

Example of secret.json:
```bash
{
    "SECRET_KEY":"123456789",
    
    "DB_NAME": "my_db_name",
    "DB_USER": "my_user",
    "DB_PASSWORD": "my_password",
    "DB_PORT":5432
}

```


## Run Migrations
```bash
python manage.py makemigrations
python manage.py migrate
```



## Upload data to DB
I can offer two options in this step: **restore from a backup file** or **run a python ETL process**

### Restore from a backup file - Recommended
Make sure you have created the database with a username and password and all migrations.
Then, execute
```bash
python manage.py dbrestore -s localhost -I ./data/pokemondb.psql

```
This backup is restored by [Django Database Backup](https://django-dbbackup.readthedocs.io/en/master/index.html) Lib.
My personal Approx. Upload Time: Few seconds.


### Run a python ETL process
The Repo has an ETL process in a custom django-admin command `(Path: apps/pokemons/management/commands/upload_data.py)`. This ETL was developed totally in python and using Django ORM 

To run the ETL execute:
```bash
python manage.py upload_data

```

My personal Approx. Upload Time: 7~8 min



## Create a superuser
```bash
python manage.py createsuperuser

```

---
**Now, all endpoints required are availables in the local api**

Just run
```bash
python manage.py runserver
```

---
# Extra Miles
In the instructions you say
> ...release the overachiever inside of you and set it free.

So, I want show some of this.
The Extra Miles developed are:

* **Update and Delete user:** Not only can create an User. The API also can Update or Delete an User.

* **Use Catch Rate to success or fail during catching request:** I developed a helper function that returns a boolean that represents if a pokemon could be captured. This function generates a list of  numbers between 1 and pokemon rate capture number. 
Then a random number is generated (1~255) and if this random number is inside the range of list, the helper function returns True 🎣, else return False🐟💨.

* **Design a clever way to manage shiny Pokemons:** Returns all shiny pokemons. This ListAPIView returns all Pokemon excluding those whose `sprite_front_shiny` attribute is not None

* **The Best pokemon in my storage:** Returns the best pokemon in the user's storage. To get the best pokemon I calculate the Pokemon's Statistics Average



**Note**: The Database ER Diagram can be finded in /helpers/


*Thanks*

**by Javier Valero**
