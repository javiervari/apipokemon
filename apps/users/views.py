import datetime

from django.contrib.sessions.models import Session

from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token
from rest_framework import status
from rest_framework.response import Response

from .api.serializers import UserTokenSerializer


class Login(ObtainAuthToken):

    def post(self, request, *args, **kwargs):

        # Serializing login request data (username and password)
        login_serializer = self.get_serializer(
            data=request.data,
            context={'request': request}
        )

        if login_serializer.is_valid():
            user = login_serializer.validated_data['user']


            if user.is_active:
                # Verify if the user have a token
                token, new = Token.objects.get_or_create(
                    user=user
                )
                
                user_serializer = UserTokenSerializer(user)

                return Response(
                    {
                        "token":token.key,
                        "user":user_serializer.data
                    }
                )

            else:
                return Response(
                    {"message": "User {} isn't authorized".format(user.email)},
                    status=status.HTTP_400_BAD_REQUEST
                )

        else:
            return Response(
                login_serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
                )


class Logout(APIView):
    
    def post(self, request, *args, **kwargs):
        try:
            # get str token in request.data
            token = request.data.get('token')
            token = Token.objects.filter(key=token).first()

            if token:
                user = token.user

                
                # All Sessions Actives
                all_sessions = Session.objects.filter(
                    expire_date__gte=datetime.datetime.now()
                        )

                for session in all_sessions:
                    session_data = session.get_decoded()
                    if user.pk == int(session_data.get('_auth_user_id')):
                        session.delete()
                
                #Eliminar token
                token.delete()

                return Response(
                    {
                        "message": "Logout successfull"
                    },
                    status=status.HTTP_200_OK
                )

            else:
                return Response(
                    {
                        "message":"Token doesn't exist"
                    },
                    status=status.HTTP_400_BAD_REQUEST

                )
            

        except Exception as e:
            return Response(
                {"message" : "Error: {}".format(e)},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR
            )
