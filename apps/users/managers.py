from django.contrib.auth.models import BaseUserManager
from apps.pokemons.models import Storage

class UserManager(BaseUserManager):
    def _create_user(self, username, full_name, password, is_staff, 
        is_superuser, **extra_fields):

        user = self.model(
            username = username,
            full_name = full_name,
            is_staff = is_staff,
            is_superuser = is_superuser,
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self.db)
        Storage.objects.create(fk_user=user)

        return user

    def create_user(self, username, full_name, password=None, 
        **extra_fields):
        return self._create_user(username, full_name, password, 
            False, False, **extra_fields)

    def create_superuser(self, username, full_name, password=None, 
        **extra_fields):
        return self._create_user(username, full_name, password, 
            True, True, **extra_fields)


