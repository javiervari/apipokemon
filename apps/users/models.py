from django.db import models
from django.contrib.auth.models import (
    AbstractBaseUser, 
    PermissionsMixin)

from .managers import UserManager

class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(
        max_length = 255, 
        unique = True)
    full_name = models.CharField(
        'Full Name', 
        max_length = 255)

    status = models.BooleanField(default=True)

    is_active = models.BooleanField(default = True)
    is_staff = models.BooleanField(default = False)
    objects = UserManager()

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['full_name']