from rest_framework import serializers
from apps.users.models import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            "username",
            "full_name",
            "password",
        ]

    def to_representation(self, instance):
        return {
            "id":instance.pk,
            "username":instance.username,
            "full_name":instance.full_name
        }

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user


class UpdateUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            "username",
            "full_name",
            "password",
        ]
        extra_kwargs = {
            'username':{'required':False},
            'full_name':{'required':False},
            'password':{'required':False},
            }

    def to_representation(self, instance):
        return {
            "id":instance.pk,
            "username":instance.username,
            "full_name":instance.full_name
        }
    
    def update(self, instance, validated_data):
        username = validated_data.get("username")
        full_name = validated_data.get("full_name")
        password = validated_data.get("password")

        if username: instance.username = username
        if full_name: instance.full_name = full_name
        if password: instance.set_password(password)

        instance.save()
        return instance
        

class UserTokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'full_name'
        ]