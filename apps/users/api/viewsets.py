from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response

from apps.users.models import User
from apps.pokemons.models import Storage
from .serializers import (
    UserSerializer, 
    UpdateUserSerializer)


class UserViewSet(viewsets.ModelViewSet):

    serializer_class = UserSerializer

    def get_queryset(self, pk=None):
        model = self.get_serializer().Meta.model
        qs = model.objects.filter(status=True)

        if pk:
            qs = qs.filter(pk=pk).first()

        return qs

    def create(self, request):
        """[​ 'POST'​ ] /login/
        Login description"""

        user_serializer = self.get_serializer(data=request.data)
        if user_serializer.is_valid():
            user_serializer.save()
                       
            return Response(
                user_serializer.data,
                status=status.HTTP_201_CREATED)
        else:
            return Response(
                user_serializer.errors, 
                status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk=None):
        """Update User: Milla Extra
        """
        user_object = self.get_queryset(pk=pk)
        user_serializer = UpdateUserSerializer(user_object, data=request.data)
        if user_object:
            if user_serializer.is_valid():
                user_serializer.save()
                return Response(
                    user_serializer.data,
                    status=status.HTTP_200_OK
                )
            else:
                return Response(
                    user_serializer.errors,
                    status=status.HTTP_400_BAD_REQUEST
                    )
        else:
            return Response(
                    {"message": "User ID {} doesn't exist ".format(pk)},
                    status=status.HTTP_400_BAD_REQUEST
                    )

    def destroy(self, request, pk=None):
        """Destroy User: Milla Extra
        """
        user_object = self.get_queryset(pk=pk)

        if user_object:
            user_object.status = False
            user_object.save()
            return Response(
                    {"message": "User ID {} deleted".format(pk)},
                    status=status.HTTP_200_OK
                    )
        else:
            return Response(
                {"message": "User ID {} doesn't exist ".format(pk)},
                status=status.HTTP_400_BAD_REQUEST
                )


