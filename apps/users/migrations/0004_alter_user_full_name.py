# Generated by Django 3.2.4 on 2021-06-12 00:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_alter_user_full_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='full_name',
            field=models.CharField(default='', max_length=255, verbose_name='Full Name'),
            preserve_default=False,
        ),
    ]
