from django.db import models


class CapturedManager(models.Manager):

    def in_storage(self, user, in_party=None):
        """Obtains all pokemons catched by an user.
        If "in_party" param is passed: Also obtains those are in the Party Group

        Args:
            user ([User]): Pokemons own user
            in_party ([bool]): to get those that are in the party.

        Returns:
            [List]: List of Captured Pokemons Objects
        """
        qs =  self.filter(fk_storage__fk_user=user)

        if in_party:
            qs = qs.filter(is_party_member=in_party)
        
        return qs.values()
