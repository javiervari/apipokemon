from django.contrib import admin
from .models import (
    Abilitie,
    Move,
    PokemonType,
    Skill,
    Pokemon,
    Statistic,
    Storage,
    Captured
    )


class PokemonAdmin(admin.ModelAdmin):
       list_display = (
        'id', 'name'
    )
    

# Register your models here.
admin.site.register(Abilitie)
admin.site.register(Move)
admin.site.register(PokemonType)
admin.site.register(Skill)
admin.site.register(Pokemon, PokemonAdmin)
admin.site.register(Statistic)
admin.site.register(Storage)
admin.site.register(Captured)