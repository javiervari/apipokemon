from django.db import models
from django.db.models import Avg

from .managers import CapturedManager


class Abilitie(models.Model):
    """Model definition for Abilities."""

    name = models.CharField(
        "Abilitie Name", 
        max_length=50)

    class Meta:
        """Meta definition for Abilities."""
        verbose_name = 'Abilitie'
        verbose_name_plural = 'Abilities'

    def __str__(self):
        """Unicode representation of Abilities."""
        return self.name


class Move(models.Model):
    """Model definition for Move."""

    name = models.CharField(
        "Name", 
        max_length=50)

    class Meta:
        """Meta definition for Move."""

        verbose_name = 'Move'
        verbose_name_plural = 'Moves'

    def __str__(self):
        """Unicode representation of Move."""
        return self.name


class PokemonType(models.Model):
    """Model definition for PokemonType."""
    name = models.CharField(
        "Name",
        max_length=50)

    class Meta:
        """Meta definition for PokemonType."""

        verbose_name = 'PokemonType'
        verbose_name_plural = 'PokemonTypes'

    def __str__(self):
        """Unicode representation of PokemonType."""
        return self.name


class Skill(models.Model):
    """Model definition for Skill."""

    name = models.CharField(
        "Skill", 
        max_length=50)

    class Meta:
        """Meta definition for Skill."""

        verbose_name = 'Skill'
        verbose_name_plural = 'Skills'

    def __str__(self):
        """Unicode representation of Skill."""
        return self.name


class Pokemon(models.Model):
    """Model definition for Pokemon."""
    name = models.CharField(
        "Name", 
        max_length=50)
    color = models.CharField(
        "Color", 
        max_length=50)
    height = models.PositiveIntegerField("Height")
    weight = models.PositiveIntegerField("Weight")
    capture_rate = models.PositiveIntegerField("Capture rate")
    flavor_text = models.TextField("Flavor text")
    sprite_back_default = models.CharField(
        "Back default sprite", 
        max_length=150,
        null=True,
        blank=True)
    sprite_back_female = models.CharField(
        "Back female sprite", 
        max_length=150,
        null=True,
        blank=True)
    sprite_back_shiny = models.CharField(
        "Back shiny sprite", 
        max_length=150,
        null=True,
        blank=True)
    sprite_back_shiny_female = models.CharField(
        "Back shiny female sprite", 
        max_length=150,
        null=True,
        blank=True)
    sprite_front_default = models.CharField(
        "Front default sprite", 
        max_length=150,
        null=True,
        blank=True)
    sprite_front_female = models.CharField(
        "Front female sprite", 
        max_length=150,
        null=True,
        blank=True)
    sprite_front_shiny = models.CharField(
        "Front shiny sprite", 
        max_length=150,
        null=True,
        blank=True)
    sprite_front_shiny_female = models.CharField(
        "Front shiny female sprite", 
        max_length=150,
        null=True,
        blank=True)
    fk_abilitie = models.ManyToManyField(Abilitie)
    fk_move = models.ManyToManyField(Move)
    fk_type = models.ManyToManyField(PokemonType)

    class Meta:
        """Meta definition for Pokemon."""

        verbose_name = 'Pokemon'
        verbose_name_plural = 'Pokemons'

    def __str__(self):
        """Unicode representation of Pokemon."""
        return self.name

    def abilitie_average(self):
        """Return the avg of all abilities

        Returns:
            [float]: Stats AVG
        """
        stats = Statistic.objects\
            .filter(fk_pokemon = self)\
            .aggregate(avg=Avg('value'))
        return float(stats['avg'])


class Statistic(models.Model):
    """Model definition for Statistics."""
    fk_pokemon = models.ForeignKey(
        Pokemon, 
        on_delete=models.CASCADE)
    fk_skill = models.ForeignKey(
        Skill, 
        on_delete=models.CASCADE)
    value = models.PositiveIntegerField("Value")

    class Meta:
        """Meta definition for Statistics."""

        verbose_name = 'Statistic'
        verbose_name_plural = 'Statistics'

    def __str__(self):
        """Unicode representation of Statistics."""
        return "{}: {}-{}".format(
            self.fk_pokemon.name,
            self.fk_skill.name,
            self.value
        )


class Storage(models.Model):
    """Model definition for Storage."""
    fk_user = models.OneToOneField(
        "users.User", 
        on_delete=models.CASCADE)

    class Meta:
        """Meta definition for Storage."""

        verbose_name = 'Storage'
        verbose_name_plural = 'Storages'

    def __str__(self):
        """Unicode representation of Storage."""
        return "Owner: {}".format(self.fk_user.full_name)


    def the_best_pokemon(self):
        """Returns the best pokemon in the Storage. By Abilities AVG
        If storage is empty: Return False
        
        Returns:
            [Pokemon]: The best pokemon in the Storage
        """
        my_pokemons = Captured.objects.filter(fk_storage=self)


        if my_pokemons.exists():
            the_best = my_pokemons[0]

            for my_pokemon in my_pokemons:
                pokemon_avg_abilitie = my_pokemon.fk_pokemon.abilitie_average()
                if pokemon_avg_abilitie >= the_best.fk_pokemon.abilitie_average():
                    the_best = my_pokemon
            
            return the_best

        else :
            return False


class Captured(models.Model):
    """Model definition for Captured."""

    nick_name = models.CharField(
        "Nick name", 
        max_length=50)
    is_party_member = models.BooleanField(
        "Is party member",
        default=False)
    fk_pokemon = models.ForeignKey(
        Pokemon, 
        on_delete=models.CASCADE)
    fk_storage = models.ForeignKey(
        Storage, 
        on_delete=models.CASCADE)
    objects = CapturedManager()
    
    class Meta:
        """Meta definition for Captured."""
        verbose_name = 'Captured'
        verbose_name_plural = 'Captureds'

    def __str__(self):
        """Unicode representation of Captured."""
        return "{}({})-{}".format(
            self.nick_name,
            self.fk_pokemon.name,
            self.fk_storage.fk_user.full_name
        )

        