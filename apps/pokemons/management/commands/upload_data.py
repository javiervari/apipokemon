import json
from datetime import datetime, time

from django.core.management.base import BaseCommand
from django.conf import settings

from apps.geography.models import Region, Location, Area
from apps.pokemons.models import (
    Abilitie,
    Move,
    PokemonType,
    Skill,
    Pokemon,
    Statistic
    )

from helpers import helpers

# Data Paths
POKEMONS_DATA_PATH = settings.BASE_DIR/"data/pokemons.json"
AREAS_DATA_PATH = settings.BASE_DIR/"data/areas.json"
LOCATIONS_DATA_PATH = settings.BASE_DIR/"data/locations.json"
REGIONS_DATA_PATH = settings.BASE_DIR/"data/regions.json"

class Command(BaseCommand):
    """ETL to upload data from json files
    """

    help = '''Uploads the initial data from json files: areas.json, \
        locations.json, regions.json and pokemons.json to DB'''


    def handle(self, *args, **kwargs):
        
        self.stdout.write(
            self.style.HTTP_NOT_MODIFIED('Starting ETL process...')
            )

        initial_time = datetime.now()

        self.stdout.write(
            self.style.HTTP_NOT_MODIFIED('Reading files:\n{}\n{}\n{}\nand {}'\
                .format(
                    POKEMONS_DATA_PATH,
                    AREAS_DATA_PATH,
                    LOCATIONS_DATA_PATH,
                    REGIONS_DATA_PATH))
                    )

        # Read data
        pokemons_json = helpers.read_json_data(POKEMONS_DATA_PATH)
        regions_json = helpers.read_json_data(REGIONS_DATA_PATH)
        locations_json = helpers.read_json_data(LOCATIONS_DATA_PATH)
        areas_json = helpers.read_json_data(AREAS_DATA_PATH)

        self.stdout.write(
            self.style.HTTP_NOT_MODIFIED(
                'Creating Regions, Locations and Areas...'
                )
            )

        # Create Regions
        regions = [ 
            Region(
                name=region['name'].title()
                ) for region in regions_json['data'] 
            ]
        Region.objects.bulk_create(regions)

        # Create Locations
        locations=[]
        for location in locations_json['data']:
            locations.append(
                Location(
                    name=location['name'].title(),
                    fk_region=Region.objects.get(
                        name=location['region'].title()
                        )
                )
            )
        Location.objects.bulk_create(locations)

        # Create Areas
        areas = []
        for area in areas_json['data']:
            areas.append(
                Area(
                    name=area['name'].title(),
                    fk_location=Location.objects.get(
                        name=area['location'].title()
                        )
                )
            )
        Area.objects.bulk_create(areas)

        # Create Pokemons
        pokemons = []

        abilities = []
        moves = []
        pokemon_types = []
        skills = []

        statistics = []

        self.stdout.write(self.style.HTTP_NOT_MODIFIED('Creating Pokemons...'))

        for pokemon in pokemons_json['data']:

            # Pokemon Instance
            pokemon_obj = Pokemon(
                name=pokemon['name'].title(),
                color=pokemon['color'],
                height=pokemon['height'],
                weight=pokemon['weight'],
                capture_rate=pokemon['capture_rate'],
                flavor_text=pokemon['flavor_text'],
                sprite_back_default=pokemon['sprites']['back_default'],
                sprite_back_female=pokemon['sprites']['back_female'],
                sprite_back_shiny=pokemon['sprites']['back_shiny'],
                sprite_back_shiny_female=pokemon['sprites']['back_shiny_female'],
                sprite_front_default=pokemon['sprites']['front_default'],
                sprite_front_female=pokemon['sprites']['front_female'],
                sprite_front_shiny=pokemon['sprites']['front_shiny'],
                sprite_front_shiny_female=pokemon['sprites']['front_shiny_female'],
            )
            pokemons.append(pokemon_obj)

            # Organizing Abilities General list
            for abilitie in pokemon['abilities']:
                if abilitie.title() not in abilities:
                    abilities.append(abilitie.title())

            # Organizing Moves General list
            for move in pokemon['moves']:
                if move.title() not in moves:
                    moves.append(move.title())

            # Organizing PokemonTypes General list
            for pokemon_type in pokemon['types']:
                if pokemon_type.title() not in pokemon_types:
                    pokemon_types.append(pokemon_type.title())

            # Organizing Skill General list
            for stat in pokemon['stats']:
                if stat['name'].title() not in skills:
                    skills.append(stat['name'].title())

        # Creating Pokemons
        Pokemon.objects.bulk_create(pokemons)

        # Creating Abiliries
        self.stdout.write(self.style.HTTP_NOT_MODIFIED('Creating Abilities...'))
        abilities = [ Abilitie(name=a) for a in abilities ]
        Abilitie.objects.bulk_create(abilities)

        # Creating Moves
        self.stdout.write(self.style.HTTP_NOT_MODIFIED('Creating Moves...'))
        moves = [ Move(name=m) for m in moves ]
        Move.objects.bulk_create(moves)

        # Creating Types
        self.stdout.write(self.style.HTTP_NOT_MODIFIED('Creating Types...'))
        pokemon_types = [ PokemonType(name=t) for t in pokemon_types ]
        PokemonType.objects.bulk_create(pokemon_types)

        # Creating Skills
        self.stdout.write(self.style.HTTP_NOT_MODIFIED('Creating Skills...'))
        skills = [ Skill(name=s) for s in skills ]
        Skill.objects.bulk_create(skills)


        self.stdout.write(
            self.style.HTTP_NOT_MODIFIED('Assigning Pokemons to Areas...')
            )

        # Create Area-Pokemon Relationships
        for area in areas_json['data']:
            area_obj = Area.objects.get(name=area['name'].title())
            
            for pokemon_name in area['pokemons']:
                area_obj.fk_pokemon.add(
                    Pokemon.objects.get(name=pokemon_name.title())
                )

        # Create (Abilites,Moves,Types, Statistics)-Pokemon Relationships
        self.stdout.write(
            self.style.HTTP_NOT_MODIFIED(
                'Assigning Abilites, Moves, Types to Pokemons...')
            )

        for pokemon in pokemons_json['data']:
            obj_pokemon = Pokemon.objects.get(name=pokemon['name'].title())

            # Abilites Relations
            for abilitie in pokemon['abilities']:
                abilitie_obj = Abilitie.objects.get(
                    name=abilitie.title()
                    )
                obj_pokemon.fk_abilitie.add(abilitie_obj)

            # Moves Relations
            for move in pokemon['moves']:
                move_obj = Move.objects.get(name=move.title())
                obj_pokemon.fk_move.add(move_obj)

            # Types Relations
            for pokemon_type in pokemon['types']:
                type_obj = PokemonType.objects.get(
                    name=pokemon_type.title()
                    )
                obj_pokemon.fk_type.add(type_obj)

            # Statistics Relations
            for stat in pokemon['stats']:
                statistics.append(
                    Statistic(
                        fk_pokemon=obj_pokemon,
                        fk_skill=Skill.objects.get(name=stat['name'].title()),
                        value=stat['value']
                    )
                )

        self.stdout.write(
            self.style.HTTP_NOT_MODIFIED('Assigning Statistics to Pokemons...')
            )
        Statistic.objects.bulk_create(statistics)
        
        final_time = datetime.now()
        total_time = final_time - initial_time

        self.stdout.write(
            self.style.SUCCESS(
                'Success! The data in the database is ready. Time: {}seg'\
                    .format(total_time.seconds))
            )


    
    
    # def handle(self, *args, **kwargs):

    #     initial_time = datetime.now()



    #     # Read data
    #     pokemons_json = helpers.read_json_data(POKEMONS_DATA_PATH)
    #     regions_json = helpers.read_json_data(REGIONS_DATA_PATH)
    #     locations_json = helpers.read_json_data(LOCATIONS_DATA_PATH)
    #     areas_json = helpers.read_json_data(AREAS_DATA_PATH)

    #     # Create Regions
    #     regions = [ 
    #         Region(
    #             name=region['name'].title()
    #             ) for region in regions_json['data'] 
    #         ]
    #     Region.objects.bulk_create(regions)

    #     # Create Locations
    #     locations=[]
    #     for location in locations_json['data']:
    #         locations.append(
    #             Location(
    #                 name=location['name'].title(),
    #                 fk_region=Region.objects.get(name=location['region'].title())
    #             )
    #         )
    #     Location.objects.bulk_create(locations)

    #     # Create Areas
    #     areas = []
    #     for area in areas_json['data']:
    #         areas.append(
    #             Area(
    #                 name=area['name'].title(),
    #                 fk_location=Location.objects.get(
    #                     name=area['location'].title()
    #                     )
    #             )
    #         )
    #     Area.objects.bulk_create(areas)

    #     # Create Pokemons
    #     pokemons = []

    #     for pokemon in pokemons_json['data']:

    #         # Pokemon Instance
    #         pokemon_obj = Pokemon(
    #             name=pokemon['name'].title(),
    #             color=pokemon['color'],
    #             height=pokemon['height'],
    #             weight=pokemon['weight'],
    #             capture_rate=pokemon['capture_rate'],
    #             flavor_text=pokemon['flavor_text'],
    #             sprite_back_default=pokemon['sprites']['back_default'],
    #             sprite_back_female=pokemon['sprites']['back_female'],
    #             sprite_back_shiny=pokemon['sprites']['back_shiny'],
    #             sprite_back_shiny_female=pokemon['sprites']['back_shiny_female'],
    #             sprite_front_default=pokemon['sprites']['front_default'],
    #             sprite_front_female=pokemon['sprites']['front_female'],
    #             sprite_front_shiny=pokemon['sprites']['front_shiny'],
    #             sprite_front_shiny_female=pokemon['sprites']['front_shiny_female'],
    #         )

    #         pokemons.append(pokemon_obj)
    #     Pokemon.objects.bulk_create(pokemons)

    #     # Create Area-Pokemon Relationships
    #     for area in areas_json['data']:
    #         area_obj = Area.objects.get(name=area['name'].title())
            
    #         for pokemon_name in area['pokemons']:
    #             area_obj.fk_pokemon.add(
    #                 Pokemon.objects.get(name=pokemon_name.title())
    #             )

    #     # Create (Abilites,Moves,Types)-Pokemon Relationships
    #     for pokemon in pokemons_json['data']:
    #         obj_pokemon = Pokemon.objects.get(name=pokemon['name'].title())

    #         # Abilites Relations
    #         for abilitie in pokemon['abilities']:
    #             abilitie_obj, _ = Abilitie.objects.get_or_create(
    #                 name=abilitie.title()
    #                 )
    #             obj_pokemon.fk_abilitie.add(abilitie_obj)

    #         # Moves Relations
    #         for move in pokemon['moves']:
    #             move_obj, _ = Move.objects.get_or_create(name=move.title())
    #             obj_pokemon.fk_move.add(move_obj)

    #         print("Types")
    #         # Types Relations
    #         for pokemon_type in pokemon['types']:
    #             print(pokemon_type,obj_pokemon.name)
    #             type_obj, _ = PokemonType.objects.get_or_create(
    #                 name=pokemon_type.title()
    #                 )
    #             obj_pokemon.fk_type.add(type_obj)

    #         print('stats')
    #         for stat in pokemon['stats']:
    #             print(stat,obj_pokemon.name)
    #             skill_obj, _ = Skill.objects.get_or_create(
    #                 name=stat['name'].title()
    #                 )

    #             Statistic.objects.create(
    #                 fk_pokemon=obj_pokemon,
    #                 fk_skill=skill_obj,
    #                 value=stat['value']
    #             )

    #     final_time = datetime.now()

    #     total_time = final_time - initial_time
    #     print("TOTAL TIME: ", total_time.seconds)







