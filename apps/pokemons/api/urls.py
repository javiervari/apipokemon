from django.urls import path

from .views import (
    PokemonRetrieveAPIView, 
    PartyApiView, 
    SwapPartyMembersApiView,
    ShinyPokemonsListAPIView,
    BestPokemonAPIView)
from .viewsets import PokemonStorageViewSet

urlpatterns = [
    path(
        'pokemons/<int:pk>/',
        PokemonRetrieveAPIView.as_view(),
        name='pokemons'
    ),
    path(
        'pokemons/own/party/',
        PartyApiView.as_view(),
        name='pokemons-own-party'
    ),
    path(
        'pokemons/own/swap/',
        SwapPartyMembersApiView.as_view(),
        name='swap-party'
    ),

    # Milla Extra
    path(
        'pokemons/shiny/',
        ShinyPokemonsListAPIView.as_view(),
        name='shiny-pokemons'
    ),

    # Milla Extra
    path(
        'pokemons/own/best/',
        BestPokemonAPIView.as_view(),
        name='best-pokemon'
    ),
]
