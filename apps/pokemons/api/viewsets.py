from rest_framework import status
from rest_framework import viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from apps.pokemons.models import Storage, Captured, Pokemon
from .serializers import CatchPokemonSerializer, PokemonShortSerializer

# PokemonStorageViewSet
class PokemonStorageViewSet(viewsets.ViewSet):
    authentication_classes = [TokenAuthentication, ]
    permission_classes = [IsAuthenticated, ]

    def list(self, request):
        """[​ 'GET'​ ] /pokemons/own/
        This service returns the complete list of Pokemon that you have 
        captured, including those that are in your party.
        """

        pokemons_in_storage = Captured.objects\
            .in_storage(user=request.user)

        pokemons_serializer = PokemonShortSerializer(
            pokemons_in_storage, many=True)

        return Response(pokemons_serializer.data, status=status.HTTP_200_OK)

    def create(self, request):
        """[​ 'POST'​ ] /pokemons/own/
        This service will be used to simulate catching a Pokémon, that is, 
        taking a species of Pokémon and turning it into a Pokemon instance in 
        your storage.
        Note: In this service it is especially important to remember that your 
        party has a maximum size of six (6) members.
        """
        serialized_data = CatchPokemonSerializer(
            data=request.data, 
            context={'request':request}
            )
        if serialized_data.is_valid():
            serialized_data.save()
            return Response(
                serialized_data.data,
                status=status.HTTP_201_CREATED)
        else:
            return Response(
                serialized_data.errors, 
                status=status.HTTP_400_BAD_REQUEST
                )

    def update(self, request, pk=None):
        """[​'PUT'​] /pokemons/own/<int:pk>/
        This service will serve to rename Pokemons that are in your storage or 
        at your party.
        """
        try:
            new_nick_name = request.data['nick_name']
            pokemon_updated = Captured.objects.get(
                pk=pk,
                fk_storage__fk_user=request.user)
            pokemon_updated.nick_name = new_nick_name
            pokemon_updated.save()

            return Response({
                "id": pokemon_updated.id,
                "nick_name": pokemon_updated.nick_name,
                "is_party_member": pokemon_updated.is_party_member,
                "specie": pokemon_updated.fk_pokemon.id
            }, status=status.HTTP_200_OK)

        except Captured.DoesNotExist:
            return Response(
                {"message":'You does not have any Pokemon captured with ID {}'\
                    .format(pk)},
                status = status.HTTP_400_BAD_REQUEST)

        except Exception as e:
            return Response(
                {"message": 'Body incorrect body. Should have the following schema {"nick_name": "Abelardo"}'},
                status = status.HTTP_400_BAD_REQUEST)

    def partial_update(self, request, pk=None):
        """[​'PATCH'​] /pokemons/own/<int:pk>/
        This service will serve to rename Pokemons that are in your storage or 
        at your party.
        """
        try:
            new_nick_name = request.data['nick_name']
            pokemon_updated = Captured.objects.get(
                pk=pk,
                fk_storage__fk_user=request.user)
            pokemon_updated.nick_name = new_nick_name
            pokemon_updated.save()

            return Response({
                "id": pokemon_updated.id,
                "nick_name": pokemon_updated.nick_name,
                "is_party_member": pokemon_updated.is_party_member,
                "specie": pokemon_updated.fk_pokemon.id
            }, status=status.HTTP_200_OK)

        except Captured.DoesNotExist:
            return Response(
                {"message":'You does not have any Pokemon captured with ID {}'\
                    .format(pk)},
                status=status.HTTP_400_BAD_REQUEST)

        except Exception as e:
            return Response(
                {"message": 'Body incorrect body. Should have the schema {"nick_name": "Abelardo"}'},
                status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None):
        """[​ 'DELETE'​ ] /pokemons/own/<int:pk>/
        This service will serve to set free a Pokemon that is in your storage 
        or at your party.
        """
        try:
            pokemon = Captured.objects.get(pk=pk)
            pokemon.delete()

            return Response(
                {'message':'Your {} ({}) has been deleted'.format(
                    pokemon.fk_pokemon.name,
                    pokemon.nick_name
                )},
                status=status.HTTP_204_NO_CONTENT)
        except Captured.DoesNotExist:
            return Response(
                {'message':'Pokemon ID {} Does not exist'.format(pk)},
                status=status.HTTP_400_BAD_REQUEST)





