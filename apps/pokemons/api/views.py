from django.forms.models import model_to_dict
from rest_framework import status
from rest_framework.generics import RetrieveAPIView, ListAPIView
from rest_framework.views import APIView
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from apps.pokemons.models import Pokemon, Storage, Captured
from .serializers import (
    PokemonSerializer, 
    SwapPartyMembersSerializer, 
    PokemonShortSerializer,
    BestPokemonSerializer)


class PokemonRetrieveAPIView(RetrieveAPIView):
    """[​ 'GET'​ ] /pokemons/<int:pk>/
    This service will aim to present the full details of a Pokemon species. 
    The id of the requested species will be in the Url
    """
    serializer_class = PokemonSerializer
    queryset = Pokemon.objects.all()


class PartyApiView(APIView):
    
    authentication_classes = [TokenAuthentication, ]
    permission_classes = [IsAuthenticated, ]

    def get(self, request):
        """[​'GET'​] /pokemons/own/party/
        This service returns the list of your party of Pokemons, it can have 
        from zero (0) elements to a maximum of six (6).
        """
        
        party_list = Captured.objects\
            .in_storage(user=request.user, in_party=True)

        party_serializer = PokemonShortSerializer(party_list, many=True)
        
        return Response(party_serializer.data, status=status.HTTP_200_OK)


class SwapPartyMembersApiView(APIView):
    """[​ 'POST'​ ] /pokemons/own/swap/
    This service is an interface to exchange, add or remove Pokemons from your 
    party.
    Remember that your party can only support a maximum of six (6) members.
    """
    authentication_classes = [TokenAuthentication, ]
    permission_classes = [IsAuthenticated, ]

    def post(self, request):
        swap_data = SwapPartyMembersSerializer(
            data=request.data,
            context={'request':request}
            )
        if swap_data.is_valid():
            entering_id = swap_data.data['entering_the_party']
            leaving_id = swap_data.data['leaving_the_party']

            if entering_id:
                entering_pokemon = Captured.objects.get(pk=entering_id)
                entering_pokemon.is_party_member = True
                entering_pokemon.save()

            if leaving_id:
                leaving_pokemon = Captured.objects.get(pk=leaving_id)
                leaving_pokemon.is_party_member = False
                leaving_pokemon.save()
            
            party_list = Captured.objects\
                .in_storage(user=request.user, in_party=True)

            party_serializer = PokemonShortSerializer(party_list, many=True)

            return Response(party_serializer.data, status=status.HTTP_200_OK)

        else:
            return Response(
                swap_data.errors,
                status=status.HTTP_400_BAD_REQUEST)


# Milla Extra
class ShinyPokemonsListAPIView(ListAPIView):
    """[​ 'GET'​ ] /pokemons/shiny/
    Returns all shiny pokemons"""
    serializer_class = PokemonSerializer
    queryset = Pokemon.objects.all().exclude(sprite_front_shiny=None)


# Milla Extra
class BestPokemonAPIView(APIView):
    """[​ 'GET'​ ] /pokemons/best/
    Returns the best pokemon in storage"""

    authentication_classes = [TokenAuthentication, ]
    permission_classes = [IsAuthenticated, ]

    def get(self, request):
        my_storage = Storage.objects.get(fk_user=request.user)
        the_best = my_storage.the_best_pokemon()

        if the_best:
            the_best_serialized = BestPokemonSerializer(
                model_to_dict(the_best),
                context={'request':request}
                )

            return Response(the_best_serialized.data, status=status.HTTP_200_OK)

        else:
            return Response(
                {"Message" : "Your storage is empty"} 
            )
        


