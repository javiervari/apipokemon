from rest_framework import serializers
from apps.pokemons.models import (
    Pokemon,
    Abilitie,
    Move,
    PokemonType,
    Statistic,
    Storage,
    Captured
)

from helpers.helpers import try_catch


class PokemonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pokemon
        fields = '__all__'

    def to_representation(self, instance):

        # stats = Statistic.objects\
        #     .filter(fk_pokemon=instance)\
        #     .values('value', name=F('fk_skill__name'))

        stats = Statistic.objects.filter(fk_pokemon=instance)
        stats = [{'name': s.fk_skill.name, 'value': s.value} for s in stats]

        return {
            'id': instance.id,
            'abilities': [i.name for i in instance.fk_abilitie.all()],
            'capture_rate': instance.capture_rate,
            'color': instance.color,
            'flavor_text': instance.flavor_text,
            'height': instance.height,
            'moves': [m.name for m in instance.fk_move.all()],
            'name': instance.name,
            'sprites': {
                'back_shiny': instance.sprite_back_shiny,
                'back_female': instance.sprite_back_female,
                'front_shiny': instance.sprite_front_shiny,
                'back_default': instance.sprite_back_default,
                'front_female': instance.sprite_front_female,
                'front_default': instance.sprite_front_default,
                'back_shiny_female': instance.sprite_back_shiny_female,
                'front_shiny_female': instance.sprite_front_shiny_female,
            },
            'stats': stats,
            'types': [t.name for t in instance.fk_type.all()],
            'weight': instance.weight

        }


class PokemonShortSerializer(serializers.ModelSerializer):
    class Meta:
        model = Captured
        fields = '__all__'

    def to_representation(self, instance):
        pokemon_specie = Pokemon.objects.get(pk=instance['fk_pokemon_id'])

        return {
            'id': instance['id'],
            'nick_name': instance['nick_name'],
            'is_party_member': instance['is_party_member'],
            'specie': {
                'id': pokemon_specie.id,
                'name': pokemon_specie.name,
                'sprites': {
                    'back_shiny': pokemon_specie.sprite_back_shiny,
                    'back_female': pokemon_specie.sprite_back_female,
                    'front_shiny': pokemon_specie.sprite_front_shiny,
                    'back_default': pokemon_specie.sprite_back_default,
                    'front_female': pokemon_specie.sprite_front_female,
                    'front_default': pokemon_specie.sprite_front_default,
                    'back_shiny_female': pokemon_specie.sprite_back_shiny_female,
                    'front_shiny_female': pokemon_specie.sprite_front_shiny_female,

                }
            }

        }


class CatchPokemonSerializer(serializers.Serializer):
    specie = serializers.CharField(max_length=50)
    nick_name = serializers.CharField(max_length=50)
    is_party_member = serializers.BooleanField()

    def to_representation(self, instance):
        
        return {
            'id': instance.id,
            'nick_name': instance.nick_name,
            'is_party_member': instance.is_party_member,
            'specie': instance.fk_pokemon.id
        }

    def validate_specie(self, value):
        try:
            Pokemon.objects.get(pk=value)
            return value
        except Pokemon.DoesNotExist:
            raise serializers.ValidationError(
                'Pokemon with ID Specie {} does not exist'.format(value)
            )

    def validate(self, validated_data):
        user = user = self.context['request'].user

        print(self.context['request'].data['is_party_member'])

        count_pokemons_in_party = Captured.objects.filter(
            fk_storage__fk_user=user,
            is_party_member=True
        ).count()

        if (count_pokemons_in_party < 6):
            return validated_data
        else:
            if self.context['request'].data['is_party_member']:
                raise serializers.ValidationError(
                    {'Error': '''Your pokemon party has reached the maximum number of members (Max 6 members). Please, catch the pokemon without include it in the party, or extract a pokemon of your party.'''}
                )
            else:
                return validated_data



    def create(self, validated_data, *args, **kwargs):
        user = self.context['request'].user

        nick_name = validated_data['nick_name']
        is_party_member = validated_data['is_party_member']
        fk_pokemon = Pokemon.objects.get(pk=validated_data['specie'])
        fk_storage = Storage.objects.get(fk_user=user)

        # Milla Extra:
        if try_catch(fk_pokemon):
            print("{} is yours =)".format(fk_pokemon.name))
            catched_pokemon = Captured.objects.create(
                nick_name=nick_name,
                is_party_member=is_party_member,
                fk_pokemon=fk_pokemon,
                fk_storage=fk_storage
                )
            return catched_pokemon
        else:
            print("{} is gone =(".format(fk_pokemon.name))
            raise serializers.ValidationError(
                {"Error" : "The {} is gone".format(fk_pokemon.name)}
            )

        
class SwapPartyMembersSerializer(serializers.Serializer):
    entering_the_party = serializers.IntegerField(min_value=1, allow_null=True)
    leaving_the_party = serializers.IntegerField(min_value=1, allow_null=True)

    def validate(self, validated_data):
        user = self.context['request'].user

        entering_id = validated_data['entering_the_party']
        leaving_id = validated_data['leaving_the_party']

        # 6 Member size validator
        if entering_id:
            count_pokemons_in_party = Captured.objects\
                .filter(fk_storage__fk_user=user, is_party_member=True)\
                .count()

            if count_pokemons_in_party == 6 and not leaving_id:
                raise serializers.ValidationError(
                    {"Error": "Your pokemon party has reached the maximum number of members (Max 6 members). Please remove a pokemon in your party and try again"}
                )

        # Pokemons validator
        if entering_id:
            entering_pokemon = Captured.objects\
                .filter(pk=entering_id, fk_storage__fk_user=user)\
                .exists()
            if not entering_pokemon:
                raise serializers.ValidationError(
                    {"Error": "You does not have any Pokemon captured with ID {}".format(
                        entering_id)}
                )

        if leaving_id:
            leaving_pokemon = Captured.objects\
                .filter(pk=leaving_id, fk_storage__fk_user=user)\
                .exists()
            if not leaving_pokemon:
                raise serializers.ValidationError(
                    {"Error": "You doesn't have any Pokemon captured with ID {}"
                     .format(leaving_id)}
                )
        return validated_data


class BestPokemonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Captured
        fields = '__all__'

    def to_representation(self, instance):
        pokemon = Captured.objects.get(
            fk_pokemon__id=instance['fk_pokemon'], 
            fk_storage__fk_user=self.context['request'].user)

        return {
            'nick_name': instance['nick_name'],
            'specie': pokemon.fk_pokemon.name,
            'is_party_member': pokemon.is_party_member,
            'avg_abilitie': pokemon.fk_pokemon.abilitie_average(),
            'type': [type.name for type in pokemon.fk_pokemon.fk_type.all()]
            }


