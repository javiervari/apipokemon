from rest_framework.routers import DefaultRouter
from .viewsets import PokemonStorageViewSet

router = DefaultRouter()

router.register(
    r'pokemons/own', 
    PokemonStorageViewSet, 
    basename='own')

urlpatterns = router.urls