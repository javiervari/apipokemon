from django.contrib import admin
from .models import Region, Location, Area

class LocationAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'name', 'fk_region'
    )

class AreaAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'name', 'fk_location'
    )
    



admin.site.register(Region)
admin.site.register(Location, LocationAdmin)
admin.site.register(Area, AreaAdmin)