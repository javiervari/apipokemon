from django.db import models


class Region(models.Model):
    """Model definition for Region."""

    name = models.CharField(
        "Name", 
        max_length=100)

    class Meta:
        """Meta definition for Region."""

        verbose_name = 'Region'
        verbose_name_plural = 'Regions'

    def __str__(self):
        """Unicode representation of Region."""
        return self.name


class Location(models.Model):
    """Model definition for Location."""

    name = models.CharField(
        "Name", 
        max_length=100)
    fk_region = models.ForeignKey(
        Region, 
        on_delete=models.CASCADE)

    class Meta:
        """Meta definition for Location."""

        verbose_name = 'Location'
        verbose_name_plural = 'Locations'

    def __str__(self):
        """Unicode representation of Location."""
        return self.name


class Area(models.Model):
    """Model definition for Area."""

    name = models.CharField(
        "Name", 
        max_length=100)
    fk_location = models.ForeignKey(
        Location, 
        on_delete=models.CASCADE)
    fk_pokemon = models.ManyToManyField("pokemons.Pokemon")
    


    class Meta:
        """Meta definition for Area."""

        verbose_name = 'Area'
        verbose_name_plural = 'Areas'

    def __str__(self):
        """Unicode representation of Area."""
        return self.name

    # TODO: Define custom methods here
