from django.urls import path

from .views import (
    RegionListAPIView, 
    RegionRetrieveAPIView, 
    LocationRetrieveAPIView,
    AreaRetrieveAPIView
    )

urlpatterns = [
    path(
        'regions/',
        RegionListAPIView.as_view(),
        name='regions'
    ),
    path(
        'regions/<int:pk>/',
        RegionRetrieveAPIView.as_view(),
        name='retrieve-region'
    ),
    path(
        'location/<int:pk>/',
        LocationRetrieveAPIView.as_view(),
        name='retrieve-location'
    ),
    path(
        'areas/<int:pk>/',
        AreaRetrieveAPIView.as_view(),
        name='retrieve-area'
    ),
]
