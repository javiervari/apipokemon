from rest_framework.generics import ListAPIView, RetrieveAPIView

from apps.geography.models import Region, Location, Area
from .serializers import (
    RegionSerializer, 
    RegionRetrieveSerializer, 
    LocationSerializer,
    AreaSerializer
    )


class RegionListAPIView(ListAPIView):
    """[​ 'GET'​ ] /regions/
    This service returns the list of Regions with their respective names and 
    id’s."""
    serializer_class = RegionSerializer
    queryset = Region.objects.all()


class RegionRetrieveAPIView(RetrieveAPIView):
    """[​ 'GET'​ ] /regions/<int:pk>/
    This service returns a Region with a list of Locations with their 
    respective names and identifiers."""
    serializer_class = RegionRetrieveSerializer
    queryset = Region.objects.all()


class LocationRetrieveAPIView(RetrieveAPIView):
    """[​ 'GET'​ ] /location/<int:pk>/
    This service returns a Location with a list of Areas with their respective 
    names and identifiers, in addition to returning the number of Pokémon 
    species that can be found in that Area."""
    serializer_class = LocationSerializer
    queryset = Location.objects.all()


class AreaRetrieveAPIView(RetrieveAPIView):
    """[​ 'GET'​ ] /areas/<int:pk>/
    This service returns an Area with a list of Pokemon species with their 
    respective names, identifiers and sprites."""
    serializer_class = AreaSerializer
    queryset = Area.objects.all()
