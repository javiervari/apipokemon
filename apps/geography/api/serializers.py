from rest_framework import serializers
from django.db.models import F, Count
from apps.geography.models import Region, Location, Area


class RegionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Region
        fields = '__all__'


class RegionRetrieveSerializer(serializers.ModelSerializer):
    class Meta:
        model = Region
        fields = '__all__'

    def to_representation(self, instance):
        locations = Location.objects\
            .filter(fk_region=instance)\
            .values('id','name')

        return {
            "id": instance.pk,
            "locations" : locations,
            "name": instance.name
        }


class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = '__all__'

    def to_representation(self, instance):
        areas = Area.objects.filter(
            fk_location=instance
        ).values(
            'id', 'name', location=F('fk_location')
        ).annotate(
            pokemon_count=Count('fk_pokemon')
        )

        return {
            'id': instance.pk,
            'areas': areas,
            'name': instance.name,
            'region': instance.fk_region.pk,
        }


class AreaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Area
        fields = '__all__'

    def to_representation(self, instance):
        area_obj = Area.objects.filter(pk=instance.pk)

        pokemon_count = area_obj.values(count=Count('fk_pokemon'))
        pokemons_list = area_obj.first().fk_pokemon.all()

        return {
            'id': instance.id,
            'pokemon_count': pokemon_count[0]['count'],
            'pokemons': [
                {
                    'id':p.pk,
                    'name':p.name,
                    'sprites': {
                        "back_default":p.sprite_back_default,
                        "back_female":p.sprite_back_female,
                        "back_shiny":p.sprite_back_shiny,
                        "back_shiny_female":p.sprite_back_shiny_female,
                        "front_default":p.sprite_front_default,
                        "front_female":p.sprite_front_female,
                        "front_shiny":p.sprite_front_shiny,
                        "front_shiny_female":p.sprite_front_shiny_female,
                    }
                } for p in pokemons_list
            ],
            'name': instance.name,
            'location': instance.fk_location.pk,
            }